// console.log('test');

// LOOPS
// while
/*
		- A while loop takes in an expression/condition
		- Expressions are any unit of code that can be evaluated to a value
		- If the condition evaluates to true, the statements inside the code block will be executed
		- A statement is a command that the programmer gives to the computer
		- A loop will iterate a certain number of times until an expression/condition is met
		- "Iteration" is the term given to the repetition of statements
	*/

  let count = 5;

  while(count !== 0) {
    count--
    // console.log(count);
  }

  number = 1;
  do {
    number++
    // console.log(number);
  } while (number < 10);


  /*
		- A for loop is more flexible than while and do-while loops. It consists of three parts:
      1. The "initialization" value that will track the progression of the loop.
      2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
      3. The "finalExpression" indicates how to advance the loop.
	*/

  /*
      - Will create a loop that will start from 0 and end at 20
      - Every iteration of the loop, the value of count will be checked if it is equal or less than 20
      - If the value of count is less than or equal to 20 the statement inside of the loop will execute
      - The value of count will be incremented by one for each iteration
  */
  let x = 'akainearlzapanta'
  let z = 0;

  for (let y = 0; y < x.length; y++) {
    if(
        x[y]=='a' || 
        x[y]=='e' || 
        x[y]=='i' || 
        x[y]=='o' || 
        x[y]=='u'
      ) {
      console.log('*');
      z++
    } else {
      console.log(x[y]);
    }
  }
    console.log("vowel count: " + z);


    // If the character of your name is a vowel letter, instead of displaying the character, display "*"
		    // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
// If the letter in the name is a vowel, it will print the *
// Print in the console all non-vowel characters in the name

for (let count = 0; count <= 20; count++) {
    if(count % 2 === 0) {
      continue;
    }

    console.log("continue and break " + count);
    if(count > 10) {
      break;
    }
}

j = 'alengandro'

for (let i = 0; i < j.length; i++) {

    console.log(j[i]);

    if(j[i].toLowerCase() === "a") {
      console.log("continue to next iteration");
      continue
    }

    if(j[i] == "d"){
      break
    }
}