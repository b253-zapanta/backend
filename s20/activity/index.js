// console.log('test');

// console.log("Hello World");

//Objective 1
// Create a function called printNumbers() that will loop over a number provided as an argument.
	//In the function, add a console to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the value provided is less than or equal to 50, terminate the loop and show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the value is divisible by 5, print the number.

      function printNumbers(number){
        console.log("The number you provided is: " + number);
      
        for (x = number + 1; x-- > 0;) {
          if(x <= 50) {
            console.log("The current value is at " + x + ". Terminating the loop.");
            break;
          }
          if(x % 10 === 0) {
            console.log("The number is divisible by 10. Skipping the number.");
            continue;
          }
          if(x % 5 === 0) {
            console.log(x);
            // continue;
          }
        }
      }


//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for (let x = 0; x < string.length; x++) {
  if (
    string[x] == 'a' ||
    string[x] == 'e' ||
    string[x] == 'i' ||
    string[x] == 'o' ||
    string[x] == 'u' 
    ){
      continue;
    }
  else {
    filteredString = filteredString + string[x]
  }
}

console.log(filteredString);




//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}