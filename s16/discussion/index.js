
// arithmetic
let x = 81, y = 9;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo operator: " + remainder);


// Assignment 

let assignmentNumber = 8;
// assignmentNumber = assignmentNumber + 2;

assignmentNumber +=2;
console.log('result of +: '+assignmentNumber);
assignmentNumber -=2;
console.log('result of -: '+assignmentNumber);
assignmentNumber *=2;
console.log('result of *: '+assignmentNumber);
assignmentNumber /=2;
console.log('result of *: '+assignmentNumber);
assignmentNumber %=2;
console.log('result of %: '+assignmentNumber);

// pemdas

let pemdas = 1 + (2-3) * (4/5);
console.log('result: '+pemdas);


// increment decrement
let z = 1;

let increment = ++z;
console.log('result: ' + increment);
console.log('result: ' + z);

increment = z++;
console.log('result: '+increment);
console.log('result:'+z);

// coercion

let xx = 'hehe';
let yy =  123;
let coercion = xx + yy;
console.log('result: '+coercion +'\n' +'type: '+ (typeof coercion));

let x1 = 123;
let x2 =  123;
let nonCoercion = x1 + x2;
console.log('result: '+nonCoercion +'\n' +'type: '+ (typeof nonCoercion));

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

let juan = 'juan';

console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log('juan'=='juan');
console.log(juan=='juan');

console.log(1===1);
console.log(1===2);
console.log(1==='1');
console.log('juan'==='juan');
console.log(juan==='juan');