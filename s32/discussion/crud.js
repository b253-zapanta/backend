directory = [
  {
    name : "Brandon",
    email : "brandon@mail.com"
  },
  {
    name : "Jobert",
    email : "jobert@mail.com"
  }
]

let http = require('http')
http.createServer((request,response)=>{
  if(request.url=="/users" && request.method=="GET"){

    response.writeHead(200, {"Content-Type": "application/json"})
    response.write(JSON.stringify(directory))
    response.end()

  }
  else if(request.url =="/users" && request.method == "POST"){
    requestBody = ""

    request.on("data",(data)=>{
      requestBody += data
    })

    request.on("end",()=>{
      // console.log(typeof requestBody)

      requestBody=JSON.parse(requestBody)
  
      newUser = {
        name : requestBody.name,
        email : requestBody.email
      }

      directory.push(newUser)
      console.log(directory)

      response.writeHead(200,{"Content-Type":"application/json"})
      response.write(JSON.stringify(newUser))
      response.end()
    })
  }
}).listen(4000)

console.log('Server running at locahost:4000')
