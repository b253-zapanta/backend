// ES6 UPDATES

//exponent operator

const firstNum = 8 ** 2 //ES6 update
console.log(firstNum);

const secondNum = Math.pow(8,2)
console.log(secondNum);

// Template Literals
// allows to write strings using the concat operator (+)

let name = 'test'
console.log('using concat (+) sample: ');
console.log('hi ' + name + ' welcome back');

// backticks
console.log('backtick sample: ');
console.log(`Hello ${name} welcome back`);

// multiline
console.log(
  `
    ${name} is here.
    next line sample here.
  `
);

/*
  - Template literals allow us to write strings with embedded JavaScript expressions
  - expressions are any valid unit of code that resolves to a value
  - "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1
const principal = 1000
console.log(`The interest on your savings is: ${principal * interestRate}`);

/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
*/

const fullName = ['juan','dela','cruz']
console.log(
  `hello ${fullName[0]} ${fullName[1]} ${fullName[2]} nice to meet you`
);

// array destructuring
const [firstName,MiddleName,lastName] = fullName

console.log(
  `hello ${firstName} ${MiddleName} ${lastName} nice to meet you`
);

/* Object destructuring
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
  syntax :
    let/const {propertyName, propertyName, propertyName} = objectName
*/

const person = {
  givenName : "Juan",
  maidenName : "Dela",
  familyName : "Cruz"
}

// const {givenName,maidenName, familyName} = person
// console.log(
//   `hello ${givenName} ${maidenName} ${familyName} ! good to see you`
// );

function getFullName({givenName, maidenName, familyName}){
    console.log(`hello ${givenName} ${maidenName} ${familyName} ! good to see you`);
}
getFullName(person)

/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

// pre-arrow function
// - Arrow functions also have implicit return which means we don't need to use the "return" keyword
/*
Syntax:
    let/const variableName = (parameterA, parameterB) => {
      statement
    }

    or (if single-line no need for curly brackets)

    let/const variableName = () => expression;
*/


// function printFullName(firstName,lastName){
  // console.log(`${firstName} ${lastName}`);
// }
const printFullName = (firstName, lastName) => {
  console.log(`${firstName} ${lastName}`);
}
printFullName("John","Smith")
const hello = () =>  { console.log('hello') }
hello()

// single
const sum = (x,y) => x + y
let total = sum(1,1)
console.log(total);

person.talk = () => "helloooooooooo"

console.log(person.talk()); 


// function with default argument value
const greet = (name = "User") => {
  return `Good morning, ${name}!`
}

console.log(greet('ariana'))

// Class-Based Object Blueprints

/*
	allows the creation/instantation of object using classes as blue prints
	
	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectpropertyA;
				this.objectPropertyB = objectpropertyB;
			}
		};

*/

class Car {
	constructor(brand, model, year){
		this.brand = brand;
		this.model = model;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


const myNewCar = new Car("Toyata", "Vios", 2021);
console.log(myNewCar);
