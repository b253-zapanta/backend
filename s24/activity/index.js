// Exponent Operator
const getCube = 2 ** 3 //ES6 update
console.log(`The cube of 2 is ${getCube} `);
// Template Literals

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const [houseNumber,street,state,zipCode] = address;

console.log(
  `I live at ${houseNumber} ${street}, ${state} ${zipCode}`
);  

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal
console.log(
  `${name} was a ${species}. He weighed at ${weight} with a measurent of ${measurement}.`
);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
  numbers.forEach(number => console.log(number))

// reduce
// reducedArray = numbers.reduce(function(acc,curr){return acc + curr}) 
let reduceNumber = numbers.reduce((acc,curr) => { return acc+curr })
console.log(reduceNumber);


// Javascript Classes
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dog1 = new Dog();

dog1.name = "Frankie";
dog1.age = 5;
dog1.breed = 'Miniature Dachshund';

console.log(dog1);



//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}