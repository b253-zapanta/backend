const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Allows access to routes defined within our application

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

mongoose.connect(
  "mongodb+srv://admin:admin123@batch253-zapanta.uonw2ah.mongodb.net/course-bookingAPI?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
const userRoute = require("./routers/userRoute");
const courseRoute = require("./routers/courseRoute");

app.use("/users", userRoute);
app.use("/courses", courseRoute);

if (require.main === module) {
  app.listen(port, () => {
    console.log(`Server running at PORT ${port}`);
  });
}

module.exports = app;
