const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

let userActive = false;
const isUserLogged = (data) => {
  if (data.id) userActive = true;
  return userActive;
};

// let userStatus = isUserLogged(auth.decode(req.headers.authorization));
// console.log(`isUserLogged : ${userStatus}`);

router.post("/addCourse", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  courseController
    .addCourse(req.body, userData)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/all", (req, res) => {
  courseController
    .getAllCourse()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/getActiveCourses", auth.verify, (req, res) => {
  courseController
    .getActiveCourses()
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.get("/:id", (req, res) => {
  courseController
    .getCourse(req.params.id)
    .then((resultFromController) => res.send(resultFromController))
    .catch((err) => res.send(err));
});

router.put("/:id", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .updateCourse(req.body, req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

router.put("/:id/updateCourse", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .updateCourse(req.body, req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

router.patch("/:id/archiveCourse", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);

    if (userData.isAdmin) {
      courseController
        .archiveCourse(req.params.id)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
    } else {
      res.send(false);
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
