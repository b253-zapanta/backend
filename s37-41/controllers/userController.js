// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/userModel");
const Course = require("../models/courseModel");

// Check if the email already exists
/*
Steps: 
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
  // The result is sent back to the frontend via the "then" method found in the route file
  return User.find({ email: reqBody.email })
    .then((result) => {
      // The "find" method returns a record if a match is found
      if (result.length > 0) {
        return true;
        // No duplicate email found
        // The user is not yet registered in the database
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

// User registration
/*
            Steps:
            1. Create a new User object using the mongoose model and the information from the request body
            2. Make sure that the password is encrypted
            3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser
    .save()
    .then((user) => {
      if (user) {
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return false;
        }
      }
    })
    .catch((err) => err);
};

module.exports.defaultPassword = (id) => {
  return User.findById(id)
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        result.password = "defaultpassword123";
        return result;
      }
    })
    .catch((err) => err);
};

module.exports.getProfile = (data) => {
  return User.findById(data.userId)
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        return result;
      }
    })
    .catch((err) => err);
};

module.exports.enroll = async (data, userData) => {
  let userId = userData.id;
  if (!userData.isAdmin) {
    let isUserUpdated = await User.findById(userId).then((user) => {
      user.enrollments.push({ courseId: data.courseId });
      return user
        .save()
        .then((user) => true)
        .catch((err) => false);
    });

    let isCourseUpdated = await Course.findById(data.courseId).then(
      (course) => {
        course.enrollees.push({ userId: userId });
        return course
          .save()
          .then((course) => true)
          .catch((err) => false);
      }
    );

    if (isUserUpdated && isCourseUpdated) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
};
