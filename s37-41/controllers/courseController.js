const Course = require("../models/courseModel");
const User = require("../models/userModel");

module.exports.addCourse = (reqBody, userData) => {
  if (userData.isAdmin == true) {
    let newCourse = new Course({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });

    return newCourse
      .save()
      .then((course) => true)
      .catch((err) => false);
  } else {
    return User.findOne({ _id: userData.id })
      .then((result) => {
        return `Current User : ${result.firstName} ${result.lastName} \n isAdmin? : ${result.isAdmin} \n Course not added, You have no authority`;
      })
      .catch((err) => err);
  }
};

module.exports.getAllCourse = () => {
  return Course.find({})
    .then((result) => result)
    .catch((err) => err);
};

module.exports.getActiveCourses = (req) => {
  return Course.find({ isActive: true })
    .then((result) => result)
    .catch((err) => err);
};

module.exports.getCourse = (courseId) => {
  return Course.findById(courseId)
    .then((result) => result)
    .catch((err) => err);
};

module.exports.updateCourse = (reqBody, courseId) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(courseId, updatedCourse)
    .then((result) => true)
    .catch((err) => err);
};

module.exports.updateCourse = (reqBody, courseId) => {
  console.log(reqBody);
  console.log(courseId);
  let updatedCourse = reqBody;

  return Course.findByIdAndUpdate(courseId, updatedCourse)
    .then((result) => {
      if (result) return { msg: "Course updated!", status: true, data: result };
      return { msg: "Course does not exist!", status: false, data: result };
    })
    .catch((err) => err);
};

module.exports.archiveCourse = (courseId) => {
  // console.log(reqBody);
  return Course.findById(courseId)
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        result.isActive = false;
        return { msg: "Course Archived!", status: true, data: result };
      }
    })
    .catch((err) => err);
};
