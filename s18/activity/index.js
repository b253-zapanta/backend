console.log('~activity 18~');

// addNum
function addNum(num1,num2) {
  sum = num1 + num2;

  console.log("Displayed sum of "+ num1 + " and " + num2 +"\n" + sum );
}
addNum(5,15);

//subNum 
function subNum (num1,num2) {
  diff = num1 - num2;
  console.log("Displayed difference of "+ num1 + " and " + num2 +"\n" + diff );
}
subNum(20,5)


// multiplyNum
function multiplyNum (num1,num2){
  // console.log(
  //   "The product of " +num1+ " and " + num2 
  // );
  return num1 * num2;
}
let product = multiplyNum(50,10)
console.log(
  "The product of 50 and 10 : \n" +
  product
  )


//divideNum 
function divideNum (num1,num2) {
  return num1 / num2;
}
let quotient = divideNum(50,10)
console.log(
  "The quotient of 50 and 10 : \n" +
  quotient
  )

//getCircleArea
// formula A = pi*r^2
let pi = 3.1416;
function getCircleArea(rad){
  return pi * rad ** 2;
}

let circleArea = getCircleArea(15)
console.log(
  "The result of getting the area of a circle with 15 radius: \n" +
  circleArea
  );

// getAverage 
// formula : Average = (Sum of Observations) ÷ (Total Numbers of Observations).
function getAverage(n1,n2,n3,n4) {
  return (n1+n2+n3+n4)/4;
}

let averageVAr = getAverage(20,40,60,80)
console.log(
  "The average of 20,40,60 and 80: \n" +
  averageVAr
);

// checkIfPassed
// formula : score/totalscore x 100
function checkIfPassed(score,total_score){
  let percent = (score/total_score) * 100;
  
  isPassed = percent >= 75;
  return isPassed
}

let isPassingScore = checkIfPassed(38,50)
console.log(
  "Is 38/50 a passing score? \n" +
  isPassingScore
  );


//Do not modify
//For exporting to test.js
try {
  module.exports = {
      addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
  }
} catch (err) {

}