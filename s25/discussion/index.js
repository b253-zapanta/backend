/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission/
/transfer of information
	- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers or typographic symbols)
	- Bytes are information that a computer processes to perform different tasks
	- Uses double quotes for property names
	- Syntax
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/
// JSON Object contains methods for parsing and converting data into a stringified JSON data format.


// Converting Data into Stringified JSON (JS Object to JSON Data Format)
	// JSON.stringify()


// {
//   "city" : "Quezon City",
//   "province" : "Metro Manila",
//   "country" : "Philippines"
// }

let batchesArr = [
  {batchName: "Batch 253"},
  {batchName: "Batch 243"}
]

console.log(batchesArr);


// The stringify method is used to convert JS objects into string/JSON Data Format
// Before sending data, it converts as array or an object to its string equivalent

let test = `[
	{
		"batchName": "Batch 253"
	},
	{
		"batchName": "Batch 243"
	}
]`

console.log(test);

console.log(JSON.parse(test));

let test1 = `{
  "name" : "john",
  "age" : "31",
  "address" : {
    "province" : "Bulacan",
    "country" : "Philippines"
  }
}`

console.log(test1);
console.log(JSON.parse(test1));