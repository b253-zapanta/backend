// console.log("Wasap!");

// Array Mutators

/*
    1. Mutator Methods
        - seeks to modify the contents of an array.
        - mutator methods are functions that mutate or change an array after they are created. These methods manipulate the original array by perfoming various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Banna", "Orange", "Mango"];

/*
 	push()
 		-adds an element at the end of an array and it returns array's Length
*/

console.log("Current Fruits Array:");
console.log(fruits);

// adding element/s
let fruitLength = fruits.push("Pineapple");
console.log(fruitLength);
console.log("Mutated Array using push() method");
console.log(fruits);

fruits.push("Avocado", "Jackfruit", "Guava", "Pomelo");
console.log(fruits);

/* 
    pop()
        - removes the last elementin our array and returns the removed element (when valu is passed in another variable)
*/

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated Array using pop() method");
console.log(fruits);
fruits.pop();
console.log(fruits);

/* 
    unshift()
        - adds one or more elements AT THE BEGINNING of an array and returns the length of the array(when value is passed in another variable).

        Syntax:
            arrayName.unshift(element);
            arrayName.unshift(elementA, elementB);
*/

let unshiftLength = fruits.unshift("Lemon");
console.log(unshiftLength);
console.log("Mutated Array using unshift() method");
console.log(fruits);

fruits.unshift("Kiwi", "Strawberry");
console.log(fruits);


/* 
    shift()
        - removes an element AT THE BEGINNING of our array and returns the removed elementwhen stored in avariable.
*/

let shiftFruit = fruits.shift();
console.log(shiftFruit);
console.log("Mutated Array using shift() method");
console.log(fruits);

/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

*/

let spliceFruit = fruits.splice(1, 2, "Cherry", "Dragon Fruit");
console.log(spliceFruit);
console.log("Mutated Array using splice() method");
console.log(fruits);

fruits.splice(5, 3);
console.log(fruits);


/*MINI ACTIVITY:
  - Create a function which will allow us to list fruits in the fruits array.
  -- this function should be able to receive a string.
  -- determine if the input fruit name already exist in the fruits array.
    *** If it does, show an alert message: "Fruit already listed on our inventory".
    *** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
  -- invoke and register a new fruit in the fruit array.
  -- log the up
-- log the updated fruits array in the console
*/




function registerFruit (fruitName) {
  let doesFruitExist = fruits.includes(fruitName);

  if(doesFruitExist) {
    alert(fruitName + " is already on our inventory")
  } else {
    fruits.push(fruitName);
    alert("Fruit is now listed in our inventory")
  }
}


/* 
    2. Non-Mutator Methods
        - these are methods/functions that do not modify or change an array they are created. These methods also do not manipulate the original array but still performs various tasks such as returning elements from an array.
*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DK']
console.log(countries);

/* 
    indexOf()
        - returns the index number of the FIRST MATCHING element found in an array. If no match was found, the result will be -1. The search process will be done from our first element proceeding to the last element.

        Syntax:
            arrayName.indexOf(searchValue);
            arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf('PH')
console.log(firstIndex);
firstIndex = countries.indexOf('PH',4)
console.log(firstIndex);
firstIndex = countries.indexOf('de')
console.log(firstIndex);

/*
	lastIndexOf()
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element.

    lastIndexOf(searchValue,startingFromIndex)

*/

let lastIndex = countries.lastIndexOf('DK',8)
console.log(lastIndex);

/* 
    slice()
        - portions/slices elements from our array and return a new array.

        Syntax:
            arrayName.slice(startingIndex);
            arrayName.slice(startingIndex, endingIndex);
*/
let sliceArr1 = countries.slice(3,4)
console.log(sliceArr1);

// Slicing elements from specified index to the last element
let sliceArr = countries.slice(-3)
console.log(sliceArr);

// -returns an array as a string separated by commas.
// 		-is used internally on JS when an object needs to be displayed as a text (like in HTMl), or when an object needs to be used as  string


// concat()
// - combines two or more arrays and returns the combined result


// join()
// - returns an array as a string.
// 		- does not change the original array.
// 		- any separator can be speciified. The default separator is comma (,).



// 3 iteration methods

// - are loops designed to perform repetitive tasks on arrays. This is useful for manipulating array data resulting in complex tasks.
// 		- normally work with a function is supplied as an argument.
// 		- aims to evaluate each element in an array.

// Syntax:
//             arrayName.forEach(function(individualElements){
//                 statement/business loginc
//             })

task = ['testinasdasdsadadg','testingasdasd1','testdadasdsading2']
task1 = ['debadadug','deadadadbug1','dasdsadebug2']
task2 = ['sleeadadping','sleesadadaping1','sledadasdasdeping2']

allTasks = task.concat(task1,task2)

allTasks.forEach(function(task) {
  // console.log(task);
})

let filteredTask = []

allTasks.forEach(function(task) {
  console.log(task);
  
  if(task.length > 15){
    filteredTask.push(task)
  }
})

console.log(filteredTask);


/*
	map()
		- Iterates on each element AND returns new array with different values depending on the result of the function's operation
		- This is useful for performing tasks where mutating/changing the elements are required
		- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
*/

// Syntax:
//             let/const resultArray = arrayName.map(function(individualElement){
//                 return statement;
//             })

let numbers = [1,2,3,4,5]
let numberMap = numbers.map(function(number){
  return number * number;
})
console.log(numbers);
console.log(numberMap);

// every()
// - checks if all elements in an array met the given condition. returns a "true" value if all elements meet the condition and "false" if otherwise.

// 	Syntax:
// 		let/const resultArray = arrayName.every(function(individualelement){
// 			return expression/condition
// 		});

// some()

/*
	
	some()
		- checks if at least one element in the array meet the given condition. Returns a "true" value is at least one element meets the given condition and false if otherwise.

	Syntax:
		let/const resultArray = arrayName.some(function(individualelement){
			return expression/condition
		});


*/

/*
	filter()
		- returns a new array that contains elements which meet the given condiiton. Returns an empty array if no elements were found that satisfy the given condition.

	Syntax:
		let/const resultArray = arrayName.filter(function(individualelement){
			return expression/condition
		});
*/

/*

	includes()
		- checks if the argument passed can be found in the array.
		- methods can be "chained" using them one after another. The result of the first methid is being used on the second method until all the "chained" methods have been resolved.

*/

// reduce()
// - Evaluates elements from left to right and returns/reduces the array into a single value
//         - The "accumulator" parameter in the function stores the result for every iteration of the loop
// - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
//         - How the "reduce" method works
//             1. The first/result element in the array is stored in the "accumulator" parameter
//             2. The second/next element in the array is stored in the "currentValue" parameter
//             3. An operation is performed on the two elements
//             4. The loop repeats step 1-3 until all elements have been worked on

//         - Syntax
//             let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
//                 return expression/operation
//             })

/*
	reduce()
		- Evaluates elements from left to right and returns/reduces the array into a single value
		- The "accumulator" parameter in the function stores the result for every iteration of the loop
		reduce()
		- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
		- How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
		Syntax:
			    let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
			        return expression/operation
			    })
*/

// Fibonacci
			//     3+3=6+4=10+5=15
// let numbers = [1, 2, 3, 4, 5];

let iteration = 0;

let reducedArray = numbers.reduce(function(acc,curr){

	// track the current iteration
	console.warn(`Current iteration: ${++iteration}`);

	console.log(`accumulator: ${acc}`);
	console.log(`currentValue: ${curr}`);

	// operation to retun/reduce the array into a single value
	return acc + curr;
});

console.log("Result of reduce() method: " + reducedArray);

let list = ["Hello", "Again", "Batch 253"];

let reducedJoin = list.reduce(function(acc, curr){
	// return acc + " " + curr;
	return `${acc} ${curr}`;
});

console.log("Result of reduce() method: " + reducedJoin);
