// Instructions that can be provided to the students for reference:
// Activity:
// 1. Create an activity.js file on where to write and save the solution for the activity.
// 2. Use the count operator to count the total number of fruits on sale.

db.fruits.count({
  onSale: true
});
// or
db.fruits.aggregate(
  [
    {
      $match: {
        onSale: true
      }
    },
    {
      $count: "fruitsOnSale"
    }
  ]
);

// 3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.count(
  {
    stock : {
        $gt : 20
      }
  }
);

//or

db.fruits.aggregate(
  [
    {
      $match: {
        stock : {
          $gt : 20
        }
      }
    },
    {
      $count: "enoughStock"
    }
  ]
);


// 4. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
  {
    $match: {
      onSale: true
    },
  },
  {
    $group: {
      _id : "$supplier_id", 
      avg_price : {
        $avg: "$price"
      },
    }
  }
]);


// 5. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
  {
    $group: {
      _id : "$supplier_id", 
      highest_price: {
        $max : "$price"
      },
    }
  }
]);

// 6. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
  {
    $group: {
      _id : "$supplier_id", 
      lowest_price: {
        $min : "$price"
      },
    }
  }
]);

// 7. Create a git repository named S30.
// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 9. Add the link in Boodle.

