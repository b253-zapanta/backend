// console.log('test');

let studentNumberA = "2023-1921"
let studentNumberB = "2023-1922"
let studentNumberC = "2023-1923"
let studentNumberD = "2023-1924"

let studentNumbers = [
  "2023-1921",
  "2023-1922",
  "2023-1923",
  "2023-1924"
]


/*
	- Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	- Arrays also provide access to a number of functions/methods that help in achieving this
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
	- Majority of methods are used to manipulate information stored within the same object
	- Arrays are also objects which is another data type
	- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
*/

let myTask = [
  'drink html',
  'eat javascript',
  'inhale css',
  'bake sass'
]

let city1 = "Tokyo", city2 = "Manila", city3 = "Berlin"
let cities = [city1,city2,city3]

console.log(myTask)
console.log(cities)

// length property


myTask.length = myTask.length-1 
// delete last item

cities.length--
// decrementation

fullName = "akainzapanta"
x = fullName.slice(0,fullName.length-1)
console.log(x);

let theBeatles = ['John','Paul','Ringo','George']
console.log(theBeatles.length);
theBeatles.length++
console.log(theBeatles.length-1);

theBeatles[4] = "Jimboker"
console.log(theBeatles);

let koponanNiEugene = ['Eugene', 'Vincent', 'Alfred', 'Dennis']
console.log(koponanNiEugene[3]);
console.log(koponanNiEugene[1]);

let member = koponanNiEugene[2]

console.log(koponanNiEugene);

console.log('Array before reassignment: ');
console.log(koponanNiEugene);

koponanNiEugene[2] = 'Jeremiah'
console.log('Array after reassignment:');
console.log(koponanNiEugene);

console.log(koponanNiEugene[koponanNiEugene.length-2]);

let newArr = []
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Tzuyu"

console.log(newArr);
newArr[1] = "Mina"
console.log(newArr);

console.log(newArr.length);
newArr[newArr.length] = "momo"
console.log(newArr);
console.log(newArr.length);

/*
	Mini Activity: (10 minutes)

	Part 1: Adding a value at the end of an array
		- Create a function which is able to receive a single argument and add the input at the end of the superheroes array.
		- Invoke and add an argument to be passed to the function.
		- Log the superheroes array in the console.

Part 2:Retrieving an element using a function.
		- Create a function which is able to receive an index number as a single argument.
		- Return the element/item accessed by the index.
		- Create a global variable named heroFound and store/pass the value returned by the function.
		- Log the heroFound variable in the console.
*/

superHeroes = ["lastikman","gagamboy","pedropenduko","larrygadon"]
function x(y) {
  superHeroes[superHeroes.length] = y
  console.log(superHeroes);
}

// x('saitama')



function retrieveElement(z) {
  return superHeroes[z]
}

let heroFound = retrieveElement(0)
console.log(heroFound);

let chessBoard = [
  ["a1","b1","c1","d1","e1","f1","g1","h1"],
  ["a2","b2","c2","d2","e2","f2","g2","h2"],
  ["a3","b3","c3","d3","e3","f3","g3","h3"],
  ["a4","b4","c4","d4","e4","f4","g4","h4"],
  ["a5","b5","c5","d5","e5","f5","g5","h5"],
  ["a6","b6","c6","d6","e6","f6","g6","h6"],
  ["a7","b7","c7","d7","e7","f7","g7","h7"],
  ["a8","b8","c8","d8","e8","f8","g8","h8"],
]

console.log(chessBoard);