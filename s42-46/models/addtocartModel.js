const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  addedOn: {
    type: Date,
    defaul: new Date(),
  },
  products: [
    {
      productId: {
        type: String,
      },
    },
  ],
});

module.exports = mongoose.model("Cart", cartSchema);
