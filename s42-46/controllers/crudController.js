const Crud = require("../models/crudModel");

// Display All CRUD Data
const crud_index = async (request, response) => {
  try {
    const getCrud = await Crud.find();
    if (!getCrud.length)
      return response.status(201).send("No crud at the moment");
    return response.status(201).json({ status: "success", data: getCrud });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

// Create New CRUD
const crud_create_post = async (request, response) => {
  let crud = new Crud(request.body);
  try {
    await crud.save();
    return response.status(201).json({ status: "success", data: crud });
  } catch (error) {
    console.log(error);
    return response.status(422).send("Crud add failed");
  }
};

//Show a particular CRUD Detail by Id
const crud_details = async (request, response) => {
  try {
    const getById = await Crud.findById(request.params.id).exec();
    if (getById)
      return response.status(201).json({ status: "success", data: getById });
    return response.status(201).json("No crud found");
  } catch (error) {
    console.log(error);
    return response.status(422).send("error");
  }
};

// Update CRUD Detail by Id
const crud_update = async (request, response) => {
  try {
    const crudUpdate = await Crud.findByIdAndUpdate(
      request.params.id,
      request.body
    );
    return response
      .status(201)
      .json({ status: "Crud updated", data: crudUpdate });
  } catch (error) {
    response.status(422).send("Crud update failed.");
  }
};

// Patch CRUD Detail by Id
const crud_patch = async (request, response) => {
  try {
    const crudUpdate = await Crud.findByIdAndUpdate(
      request.params.id,
      request.body
    );

    return response.status(200).json("Crud updated");
  } catch (error) {
    response.status(422).send("Crud update failed.");
  }
};

// Delete CRUD Detail by Id
const crud_delete = (request, response) => {
  Crud.findById(request.params.id, function (err, crud) {
    if (!crud) {
      response.status(404).send("Crud not found");
    } else {
      Crud.findByIdAndRemove(request.params.id)
        .then(function () {
          response.status(200).json("Crud deleted");
        })
        .catch(function (err) {
          response.status(400).send("Crud delete failed.");
        });
    }
  });
};

module.exports = {
  crud_index,
  crud_details,
  crud_create_post,
  crud_update,
  // crud_patch,
  crud_delete,
};
