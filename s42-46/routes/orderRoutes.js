const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

// productController
router.get("/orders", orderController.order_display);
router.post("/orderCheckout", orderController.order_checkout);

module.exports = router;
