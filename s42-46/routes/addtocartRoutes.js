const express = require("express");
const router = express.Router();
const addtocartController = require("../controllers/addtocartController");

// crudController
router.get("/cart", addtocartController.cart_index);
router.post("/saveCart", addtocartController.cart_create_post);

module.exports = router;
