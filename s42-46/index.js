const dotenv = require("dotenv").config();
const express = require("express");
const cors = require("cors");
const connection = require("./db");
const app = express();
const PORT = process.env.PORT || 4000;

/* ====================== Database connection ==========================
----------------------------------------------------------------------*/
connection(); //db.js
/*---------------------------------------------------------------------
=====================================================================*/

/* ====================== Middleware ==================================
---------------------------------------------------------------------*/
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  res.locals.path = req.path;
  next();
});
/*--------------------------------------------------------------------
===================================================================*/
/* ====================== Listen port ===================================
----------------------------------------------------------------------*/
app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));
/*--------------------------------------------------------------------
===================================================================*/

/* ======================= Routes ====================================
-------------------------------------------------------------------*/
//routes path
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const addtocartRoutes = require("./routes/addtocartRoutes");

// routes
app.use("/api", userRoutes);
app.use("/api", productRoutes);
app.use("/api", orderRoutes);
app.use("/api", addtocartRoutes);
//app.use("/api/auth", authRoute);
/*-------------------------------------------------------------------
==================================================================*/
