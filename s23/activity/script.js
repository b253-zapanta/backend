// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals
let trainer = {
  // Initialize/add the given object properties and methods
  // Properties
  name : 'Ash Ketchum',
  age : 10,
  friends : {
    hoenn : ['may', 'max'],
    kanto : ['brock', 'misty']
  },
  pokemon : ['Pikachu','Charizard','Squirtle','Bulbasaur'],
  // Methods
  talk : function () {
    return 'Pikachu! I choose you!'
  }
}
// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
trainerName = trainer.name
console.log('Result of dot notation: ');
console.log(trainerName);

// Access object properties using square bracket notation
trainerPokemon = trainer['pokemon']
console.log('Result of bracket notation: ');
console.log(trainerPokemon);

// Access the trainer "talk" method
console.log("Result of talk method: " + trainer.talk());


// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name,level){
  this.name = name
  this.level = level
  this.health = 2 * level
  this.attack = level

  this.tackle = function(target){
    console.log(this.name + " tackled " + target.name);
    targetHealth = target.health - this.attack
    console.log(target.name + "'s health is now reduced to " + targetHealth) ;
    if(targetHealth <= 0 ){
      tartgetPokemon = target.name
      this.faint()
    }
  }
  this.faint = function(){
    console.log(tartgetPokemon + ' has fainted.')
  }
}


// Create/instantiate a new pokemon
let pikachu = new Pokemon('Pikachu',12)
console.log(pikachu);
// Create/instantiate a new pokemon
let geodude = new Pokemon('Geodude',8)
console.log(geodude);
// Create/instantiate a new pokemon
let mewtwo = new Pokemon('Mewtwo',100)
console.log(mewtwo);

// Invoke the tackle method and target a different object
geodude.tackle(pikachu)
// Invoke the tackle method and target a different object
mewtwo.tackle(geodude)






//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}