/*
    OBJECTS 
        - is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.
*/

// Object Literals
    /*
        - one of the methods in creating objects

        Syntax: 
            let onjectName = {
                keyA/propertyA: valueA,
                keyB/propertyB: valueB,
            }
    */

let cellphone = {
  name: "Nokia 3210",
  manufactureDate: 1999
}

console.log('result from creating object using oject literals: ');
console.log(cellphone);
console.log(typeof cellphone);


// Creating Objects Using a Constructor Function (Object Constructor)
/*
    Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

    Syntax:
        function ObjectName(valuea, valueB){
            this.keyA = valueA,
            this.keyB = valueB
        }
*/

// Syntax:
//         function ObjectName(param_value, valueB){
//             this.keyA = valueA,
//             this.keyB = valueB
//         }

//         let variableName = new function ObjectName(args_valueA, valueB);
//         console.log(variableName);

//         Note: Do not forget the "new" keyword when creating a new object

function Laptop (brand, manufactureDate) {
  this.brand = brand,
  this.manufactureDate = manufactureDate
}

let laptopJanie = new Laptop("Lenovo", 2008)
console.log('result of creating objects using constructor function: ');
console.log(laptopJanie);

let laptopOwen = new Laptop("Lenovo", [2020,2021])
console.log('result of creating objects using constructor function: ');
console.log(laptopJanie);


// Create empty objects as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer);
console.log(myComputer);

/*
	Mini Activity:
		- Create an object constructor function to produce 2 objects with 3 key-value pairs.
		- Log the 2 new objects in the console and send SS in our batch hangout.
*/

function sinigangLuto(sinigangMeat,sinigangType,lutoTime){
  this.sinigangMeat = sinigangMeat,
  this.sinigangType = sinigangType,
  this.lutoTime = lutoTime
}

let sinigang1 = new sinigangLuto('hipon','sampaloc',30);
console.log(sinigang1);
let sinigang2 = new sinigangLuto('isda','talbos',30);
console.log(sinigang2);
let sinigang3 = new sinigangLuto('pork','bayabas',180);
console.log(sinigang3);


// 


// Accessing Object Properties
	// using dot notation
  console.log("Result from dot notation: " + laptopJanie.brand);
	// using square bracket notation
console.log("Result from square bracket notation: " + laptopOwen.brand);

let deviceArr = [laptopJanie, laptopOwen];
console.log(deviceArr)

// dot notation
console.log(deviceArr[0].manufactureDate);
// square bracket notation
console.log(deviceArr[0]["manufactureDate"]);


// initializing object
let car = {}
console.log(car);
// Adding Object properties

car.name = "Honda Civic"
console.log('Results from adding properties using dot notation: ');
console.log(car);

/* Using square bracket notation for adding object properties
      - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
      - This also makes names of object properties to not follow commonly used naming conventions for them
		*/

car["manufacture date"]=2019
console.log(car["manufacture date"]); //2019
console.log(car["Manufacture date"]); //undefined
console.log(car.manufactureDate); //undefined
console.log('Results from adding properties using square bracket notatopn:')
console.log(car);

// Deleting Object Properties
delete car["manufacture date"]
console.log('Result from deleting properties');
console.log(car);

// Re-assigning object properties (Update)
car.name = "Tesla"
console.log('result from reassigning property: ');
console.log(car);

/* Object Methods
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/

let person = {
  name : 'pedro',
  age : 25,
  talk: function() {
    console.log("Hello! ako si " + this.name);
  }
}

console.log(person);
console.log('Result from object method: ');
person.talk()

person.walk = function(){
  console.log(this.name + " Have walked 25 steps forward.");
}

person.walk()

let friend = {
  firstName : 'Maria',
  lastName : 'Dela Cruz',
  address : {
    city: "Austin, Texas",
    country: "US"
  },
  emails: ["maria143@mail.com", "maria_bosxz@mail.com"],
  introduce : function(){
    console.log("Hello! My name is " + this.firstName + " " + this.lastName + "." + " I lived in " + this.address.city + ", " + this.address.country+ ".");
  }
}

friend.introduce()

let myPokemon = {
  name : "Charizard",
  level : 3,
  health : 100,
  attack : 50,
  tackle : function(){
    console.log("This pokemon tackled targetPokemon");
    console.log("TargetPokemon's health is now reduced to _targetPokemonHealth_");
  },
  faint: function(){
    console.log('Pokemon fainted.');
  }
}

console.log(myPokemon);

// Creating an object constructor instaed will help wtih this process
function Pokemon(name, level) {
  // properties
  this.name = name
  this.level = level
  this.health = 2 * level
  this.attack = level
  
  // method
  this.tackle = function(target){
    console.log(this.name + " tackled " + target.name);
    console.log(target.name) + "'s health is now reduced to" + target.health;
  }

  this.faint = function() {
    console.log(this.name + 'fainted.');
  }
}

// creates new instances of the "pokemon" object with their unique properties
let pikachu = new Pokemon("Pikachu", 16)
let charmander = new Pokemon("Charmander", 8)

pikachu.tackle(charmander)