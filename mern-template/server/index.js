require("dotenv").config();
const express = require("express");
const cors = require("cors");
const connection = require("./db");
const app = express();
const PORT = process.env.PORT || 4000;

// database connection
connection();

// middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  res.locals.path = req.path;
  next();
});

// listening on port
app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));

//routes path
const crudRoutes = require("./routes/crudRoutes");

// routes
app.use("/api", crudRoutes);
//app.use("/api/auth", authRoute);
