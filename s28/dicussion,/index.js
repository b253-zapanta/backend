// CRUD Operations
  /*
      - CRUD operations are the heart of any backend application.
      - Mastering the CRUD operations is essential for any developer.
      - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
      - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
  */
//  insert (Create)
      db.user.insertOne({
        "firstName": "Akain",
        "lastName": "Zapanta",
        "mobileNumber": "+639123456789",
        "email": "akainzapanta@mail.com",
        "conpany": "Zuitt"
      });

      db.user.insertMany([
        {
          firstName: "Stephen",
          lastName: "Hawking",
          age: 76,
          contact: {
            phone: "87654321",
            email: "stephenhawking@mail.com"
          },
          courses: {
            courses: ["Python","React","PHP","CSS"],
            department: "none"
          }
        },
        {
          firstName: "Neil",
          lastName: "Armstrong",
          age: 82,
          contact: {
            phone: "97654321",
            email: "neilarmstrong@mail.com"
          },
          courses: {
            courses: ["React","Laravel","Sass"],
            department: "none"
          }
        }
      ]);

// Find (Read/Retrieve)
// syntax :
      db.collectionName.find(); //find all 
      db.collectionName.find({field:value});


      //Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true
	*/

  // Syntax:
  //       - db.collectionName.find() - find all
  //       - db.collectionName.find({field: value}); - all document that will match the criteriea
  //       - db.collectionName.findOne({field: value}) - first document that will mathc the criteria
  //       - db.collectionName.findOne({}) - find first document

  db.courses.insertMany([
    {
      name: "Javascript 101",
			price: 5000,
			description: "Introduction to Javascript",
			isActive: true
    },
    {
      name: "HTML 101",
			price: 2000,
			description: "Introduction to HTML",
			isActive: true
    }
  ]);

  // Update
  /*
    Syntax:
        db.collectionName.updateOne(
            {
                field: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );
*/

db.collectionName.updateOne(
  {
    test:"test",
  },
  {
    $set: 
    {
      firstName:"Bill",
      lastName:"Gates"
    }
  }
);
db.collectionName.updateMany({

});


db.collectionName.updateOne(
{
  firstName: "Neil"
},
{
  $set : 
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "97654321",
      email: "neilarmstrong@mail.com"
    },
    courses:{
      courses: ["React","Laravel","Sass"]
    },
    department: "none"
  }
}
);


/*
	Mini Activity:
		- Use updateOne() to change the isActive status of a course into false.
		-Use the updateMany() to set and add “enrollees” with a value of 10.
*/

db.courses.updateOne(
  {
    name:"Javascript 101",
  },
  {
    $set: 
    {
      isActive: false
    }
  }
);

db.courses.updateOne(
  {
    name:"HTML 101",
  },
  {
    $set: 
    {
      isActive: false
    }
  }
);

db.courses.updateMany(
  {},
  {
    $set : {
      "enrollees" : 10
    }
  }
);

//remove field

db.collectionName.updateMany(
    {},
    {
      $unset : {
        test: "test",
        test1: "test1"
      }
    }
  );

// delete document
/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})
*/

db.user.deleteOne({
  "company": "microsoft"
});

db.users.deleteMany({
  "dept": "none"
})