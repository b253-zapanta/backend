jsonLink = "https://jsonplaceholder.typicode.com/todos"

// =========================================== //
// fetch request
fetch(jsonLink)
.then(response => {
  return response.json() 
})
.then(function res(data) {
  console.log(data)
  // console.log(JSON.stringify(data))
  data.map(getTitles)
})

// get titles
function getTitles(data){
  console.log(data.title);
}

// =========================================== //

// get single 
value = 1 //set value here
function getSingle(value){
  fetch(jsonLink+'/'+value)
  .then(response => {
    return response.json() 
  })
  .then(data => console.log(data))
}

getSingle(value) //invoke 

// =========================================== //
// get title
value = 1 //set value here

function getStatus(value){
  fetch(jsonLink+'/'+value)
  .then(response => {
    return response.json() 
  })
  .then(function res(data) {
    console.log(data)
    console.log(
      `
      item: ${jsonLink+value}
      title: ${data.title}
      completed?: ${data.completed}
      `
    )
  })
}

getStatus(value) //invoke 
// =========================================== //

// create post method todo
function createTodo(title,boolStatus,userId){
  fetch(jsonLink,{
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      'completed': boolStatus,
      'title' : title,
      'userId' : userId
    })
  })
  .then(response => response.json())
  .then(json=>console.log(json))
}

createTodo('Todo title test',false,2312) //invoke

// =========================================== //

// updatemethod todo
function updateTodo(title,desc,boolStatus,userId,todoID){
  fetch(jsonLink+'/'+todoID,{
    method: "PUT",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      'status': boolStatus,
      'title' : title,
      'userId' : userId,
      'dateCompleted' : new Date(),
      'description' : desc
    })
  })
  .then(response => response.json())
  .then(json=>console.log(json))
}
updateTodo('Todo title1 test1','desc test',true,2312,1) //invoke

// =========================================== //

// patch todo 

function patchTodo(field,value,todoID){
fetch(jsonLink+'/'+todoID,{
    method: "PATCH",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      field: value,
    })
  })
  .then(response => response.json())
  .then(json=>console.log(json))

}
patchTodo('title','title patch test',1) //invoke

// =========================================== //

// DELETE
todoID = ''
function deleteTodo(todoID){
  fetch(jsonLink+'/'+todoID,{
    method: "DELETE"
  })
  .then(response => response.json())
  .then(json=>console.log(json))
}
deleteTodo(1)
// =========================================== //