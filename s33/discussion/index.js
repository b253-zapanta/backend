// Javascript is by default is synchronous meaning that only one statement is executed at a time

for(i=0; i<=5;i++) {
  console.log(i);
}
console.log('hello');

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

link = "https://jsonplaceholder.typicode.com/posts"

fetchResponse = fetch(link).then(response=> {
  return response.status
})

console.log(fetchResponse);

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise

fetch(link)
.then(response => {
  return response.json() 
})
.then(data => console.log(data));


// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

// waits for the "fetch" method to complete then stores the value in the "result" variable

async function fetchData(){
  result = await fetch(link)

  console.log(result);
  console.log(typeof result);
  console.log(result.body);

  json = await (result.json())
  console.log(json);
}

fetchData()

// ============================================================ //

fetch(link+'/1')
.then(response => {
  return response.json() 
})
.then(data => console.log(data));

// ============================================================ //

fetch(link,{
  method: "POST",
  headers: {
    "Content-type": "application/json"
  },
  body: JSON.stringify({
    title: "New post",
    body: "Hello world!",
    userId: 1
  })
})
.then(response => response.json())
.then(json=>console.log(json))


// Updates a specific post following the Rest API (update, /posts/:id, PUT)

fetch(link+'/1',{
  method: "PATCH",
  headers: {
    "Content-type": "application/json"
  },
  body: JSON.stringify({
    title: "update post",
    body: "update body",
  })
})
.then(response => response.json())
.then(json=>console.log(json))


fetch(link+'/1',{
  method: "DELETE"
})
.then(response => response.json())
.then(json=>console.log(json))