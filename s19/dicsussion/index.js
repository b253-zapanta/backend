console.log('test');

// if else, else
let numG = 0;
let numH = -1;

if(numG < 0) {
  console.log('Hello');
} else if(numH > 0) {
  console.log('World');
} else {
  console.log('again');
}

function determineTyphoonIntensity(windSpeed){
  if(windSpeed <30 ){
      return 'No typhoon yet'
  }else if(windSpeed <=61 ){
    return 'tropical storm detected'
  }else if(windSpeed >= 62 && windspeed <= 119 ) {
    return 'asdasdasdasd'
  }
  else{
    return 'asdasdasdasd'
  }
}

/* 
      - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
      - Values are considered true unless defined otherwise
      - Falsy values/exceptions for truthy:
          1. false
          2. 0
          3. -0
          4. ""
          5. null
          6. undefined
          7. NaN
*/
/* 
      - If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
      - Expressions are any unit of code that can be evaluated to a value
*/

if(true) {
  console.log("Truthy");
}
if(1){
  console.log("Truthy");
}
if([]){
  console.log("Truthy");
}

// falsy examples
if(false){
  console.log("falsy");
}
if(0){
  console.log("falsy");
}
if(undefined){
  console.log("falsy");
}

// conditional (Ternary) operator
/*
	- The Conditional (Ternary) Operator takes in three operands:
	    1. condition
	    2. expression to execute if the condition is truthy
	    3. expression to execute if the condition is falsy
	- Can be used as an alternative to an "if else" statement
	- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
	- Commonly used for single statement execution where the result consists of only one line
- Commonly used for single statement execution where the result consists of only one line of code
	- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
  syntax :
  (expression) ? if true : if false;
*/

// single statemeny execution
let ternaryResult = (1 < 18) ? true : false

/* 
    - Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
    - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
- The "expression" is the information used to match the "value" provided in the switch cases
    - Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
    - Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
- The "break" statement is used to terminate the current loop once a match has been found
    - Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a match was found
    

    Syntax:
        switch (expression) {
            case value:
                statement;
                break;
            default:
                statement;
                break;
        }
*/

let day = prompt('what day of the week is it today?').toLocaleLowerCase();
console.log(day);

switch(day) {
  case 'monday' :
  console.log('The color of the day is red');
  break;
  case 'tuesday' :
  console.log('The color of the day is orange');
  break;
  case 'wednesday' :
  console.log('The color of the day is yellow');
  break;
  case 'thursday' :
  console.log('The color of the day is green');
  break;
  case 'friday' :
  console.log('The color of the day is blue');
  break;
  case 'saturday' :
  console.log('The color of the day is indigo');
  break;
  case 'sunday' :
  console.log('The color of the day is violet');
  break;
  default : 
  console.log('Please input a valid day');
  break;
}

function x () {

  try {
    alert('test')
  } catch(error){
    console.log(typeof error);

    console.warn(error.message);
  }finally {
    alert('finnaly')
  }
}