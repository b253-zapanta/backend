const express = require("express");
const router = express.Router();
const crudController = require("../controllers/crudController");

// crudController
router.get("/crud", crudController.crud_index);
router.post("/saveCrud", crudController.crud_create_post);
router
  .route("/crud/:id")
  .patch(crudController.crud_update)
  .put(crudController.crud_update)
  .delete(crudController.crud_delete)
  .get(crudController.crud_details);

module.exports = router;
