const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

// taskController
router.get("/task", taskController.task_index);
router.post("/saveTask", taskController.task_create_post);
router
  .route("/task/:id")
  .patch(taskController.task_update)
  .put(taskController.task_update)
  .delete(taskController.task_delete)
  .get(taskController.task_details);

module.exports = router;
