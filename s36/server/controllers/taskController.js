const Task = require("../models/taskModel");

// Display All task Data
const task_index = (req, res) => {
  Task.find(function (err, tasks) {
    res.json(tasks);
  });
};

// Create New task
const task_create_post = (req, res) => {
  let task = new Task(req.body);
  task
    .save()
    .then((task) => {
      res.send(task);
    })
    .catch(function (err) {
      console.log(err);
      res.status(422).send("task add failed");
    });
};

// Show a particular task Detail by Id
const task_details = (req, res) => {
  Task.findById(req.params.id, function (err, task) {
    if (!task) {
      res.status(404).send("No result found");
    } else {
      res.json(task);
    }
  });
};

// Update task Detail by Id
const task_update = (req, res) => {
  Task.findByIdAndUpdate(req.params.id, req.body)
    .then(function () {
      res.status(201).json({ status: "task updated", data: req.body });
    })
    .catch(function (err) {
      res.status(422).send("task update failed.");
    });
};

// Delete task Detail by Id
const task_delete = (req, res) => {
  Task.findById(req.params.id, function (err, task) {
    if (!task) {
      res.status(404).send("task not found");
    } else {
      task
        .findByIdAndRemove(req.params.id)
        .then(function () {
          res.status(200).json("task deleted");
        })
        .catch(function (err) {
          res.status(400).send("task delete failed.");
        });
    }
  });
};

module.exports = {
  task_index,
  task_details,
  task_create_post,
  task_update,
  task_delete,
};
