console.log('halo');

//[ SECTION ] Functions
  // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
  // Functions are mostly created to create complicated tasks to run several lines of code in succession
  // They are also used to prevent repeating lines/blocks of codes that perform the same task/function
  //Function declarations
  //(function statement) defines a function with the specified parameters.

/*
Syntax:
    function functionName() {
        code block (statement)
    }
*/
// function keyword - used to defined a javascript functions
// functionName - the function name. Functions are named to be able to use later in the code.
// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


//[ SECTION ] Function Invocation
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".

//Function Declarations
  //A function can be created through function declaration by using the function keyword and adding a function name.
  //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
  //declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.
  //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declation

  //Function Expression
  //A function can also be stored in a variable. This is called a function expression.

  //A function expression is an anonymous function assigned to the variableFunction

  //Anonymous function - a function without a name.

    // variableFunction();
    /* 
      error - function expressions, being stored 
      in a let or const variable, cannot be hoisted.
    */

  let variableFunction = function(){
    console.log('hello again');
  }

  variableFunction();


  /*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/

function showNames() {
  var functionVar = "Blossom";
  let functionLet = "Bubbles";
  const functionConst = "Buttercup";

  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);

}

showNames();

  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);

function myNewFunc(){
  let name = "Jane";

  function nestedFunc(){
    let nestedName = "john";

    console.log(name);
  }

  nestedFunc();
}

myNewFunc();

//[ SECTION ] Using alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users. 
  //As opposed to a console.log() which only shows the message on the console. 
  //It allows us to show a short dialog or instruction to our user. 
  //The page will wait until the user dismisses the dialog.

function showSampleAlert(){
  alert("hello, person!");
};

showSampleAlert();

//prompt() allows us to show a small window at the of the browser to gather user input. 
//It, much like alert(), will have the page wait until the user completes or enters their input. 
//The input from the prompt() will be returned as a String once the user dismisses the window.

//console.log(sampleNullPrompt);
//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().
//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().


let fName = prompt("Please enter your firstname", "");
let lName = prompt("Please enter your lastname", "");

console.log("welcome to my page!" );
console.log("Hello, "+ fName + " " +lName +"!")

