console.log("Hello world");

/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

  function printUserInfo(){
    let fullName = "John Doe.";
    let age = "25";
    let location = "123 street, Quezon City";
    let catName = "Joe";
    let dogName = "Danny"

    console.log(
      "Hi there, I'm "+ fullName +
      "\nI am " + age + " years old." +
      "\nI live in " + location +
      "\nI have a cat named " + catName +"."+
      "\nI have a dog named " + dogName +"."
      );
  }
printUserInfo();
/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

  
  function printFiveBands(){
    let bands = ["The Beatles","Taylor Swift","The Eagles","Rivermaya","Eraserheads"];
    // should loop here but its not discussed yet
    console.log(
      bands[0] + "\n" +
      bands[1] + "\n" +
      bands[2] + "\n" +
      bands[3] + "\n" +
      bands[4] + "\n" 
    );
  }

  printFiveBands();

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

  let printFiveMovies  = function x () {
    console.log(
      "Lion King\n" +
      "Howl's Moving Castle\n" +
      "Meet the Robinsons\n" +
      "School of Rock\n" +
      "Sprited Away\n" 
    );
  }

  printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
  console.log(friend2);
  console.log(friend3);
	// console.log(true); 
	// console.log(friends); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);





//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}